# 步步高家教机S1W解除第三方软件安装限制工具

## 关于工具包
- 作者：洋葱落叶
- 工具包版本：V3.5

**如果您转载本人的工具包，请务必保留原作者信息**

**本工具包遵循CC BY-NC-SA 4.0协议**

**作者信息和最新版本信息请 以[Github](https://github.com/ycly2333/EEBBK_package_tool/blob/main/S1W.md)页面为准**

## 关于步步高家教机解除第三方软件安装限制免责声明
*协议更新日期：2022年2月6日*
1. 所有已经解除第三方软件安装限制的家教机都可以恢复到解除限制前之状态。
2. 解除第三方软件安装限制后，家教机可以无限制地安装第三方软件，需要家长加强对孩子的监管力度，避免孩子沉迷网络，影响学习；家教机自带的学习功能不受影响。
3. 您对家教机进行解除第三方软件安装限制之操作属于您的自愿行为，若在操作过程中由于操作不当等自身原因，导致出现家教机无法正常使用等异常情况，以及解除软件安装限制之后产生的一切后果将由您本人承担！
4. 如果您使用本工具包对家教机进行解除第三方软件安装限制之操作，即默认您同意本《免责声明》。

## 校验码
**MD5**：ef55ddf476c0dce9273cfd1e9c420f24

**CRC-32**：58ba3ab7

**SHA-1**：0896e3022007a5907a4a0d98fed366a738f993ad

**SHA-256**：e3e1d1b4e4b4d2ce3d3a9e994ba09f31cf4b3397d5766c8276fa91bddb0b1288

## 本工具已内置视频教程

## 下载地址
[EEBBK BOOM](https://eebbk.com.cn/s1w.html)
