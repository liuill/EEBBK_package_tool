# 步步高家教机解除第三方软件安装限制工具
## [QQ群](Group.md)
## 关于步步高家教机解除第三方软件安装限制免责声明
*协议更新日期：2022年2月6日*
1. 所有已经解除第三方软件安装限制的家教机都可以恢复到解除限制前之状态。
2. 解除第三方软件安装限制后，家教机可以无限制地安装第三方软件，需要家长加强对孩子的监管力度，避免孩子沉迷网络，影响学习；家教机自带的学习功能不受影响。
3. 您对家教机进行解除第三方软件安装限制之操作属于您的自愿行为，若在操作过程中由于操作不当等自身原因，导致出现家教机无法正常使用等异常情况，以及解除软件安装限制之后产生的一切后果将由您本人承担！
4. 如果您使用本工具包对家教机进行解除第三方软件安装限制之操作，即默认您同意本《免责声明》。
### [屏蔽系统更新＆家长管理教程](https://kdocs.cn/l/cune7WAK6oZX)
## 支持机型
- [H8SM H9A H9S S1Pro H20](https://eebbk.com.cn/mt8167.html)
- [S3 Pro＆S3 Prow](https://eebbk.com.cn/s3pro.html)
- [S3 Pros](https://eebbk.com.cn/s3pros.html)
- [S5](https://eebbk.com.cn/s5.html)
- [S5 Pro](https://eebbk.com.cn/s5pro.html)
- [S6](https://eebbk.com.cn/s6.html)
- [S1A](S1A.md)
- [S1W](https://eebbk.com.cn/s1w.html)
- [H8A](H8A.md)
